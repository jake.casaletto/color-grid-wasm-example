use wasm_bindgen::Clamped;
use web_sys::ImageData;

use crate::{
    logic::{coordinates::CartesianCoordinate, grid::ColorGrid},
    renderer::BrowserWindow,
};

pub struct RadialGridApp {
    pub window: BrowserWindow,
    pub grid: ColorGrid,
}

impl RadialGridApp {
    pub fn new(width: u32, height: u32) -> Self {
        let grid = ColorGrid::new(width as u64, height as u64);
        let window = BrowserWindow::new(width, height);
        Self { window, grid }
    }

    pub fn render(&self) {
        let mut data = Vec::new();
        // TODO: for now assume same height/width of canvas and image but eventually it'll need to get treated separately as ratios
        for x in 0..self.grid.width {
            for y in 0..self.grid.height {
                let (r, g, b, a) = self
                    .grid
                    .colors
                    .get(&CartesianCoordinate { x, y })
                    .unwrap()
                    .get_rgba();
                data.push(*r);
                data.push(*g);
                data.push(*b);
                data.push(*a);
            }
        }
        let new_image = ImageData::new_with_u8_clamped_array_and_sh(
            Clamped(&data),
            self.window.render_surface.width,
            self.window.render_surface.height,
        )
        .unwrap();
        self.window
            .render_surface
            .context
            .put_image_data(&new_image, 0.0, 0.0)
            .unwrap();
    }
}
