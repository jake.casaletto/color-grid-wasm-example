#![allow(
    clippy::implicit_return,
    clippy::iter_nth_zero,
    clippy::match_bool,
    clippy::missing_errors_doc,
    clippy::non_ascii_literal,
    clippy::wildcard_imports,
    clippy::module_name_repetitions,
    incomplete_features
)]
#![warn(
    clippy::all,
    clippy::nursery,
    clippy::pedantic,
    clippy::unreadable_literal,
    rust_2018_idioms
)]
#![deny(
    clippy::pedantic,
    clippy::float_cmp_const,
    clippy::unwrap_used,
    clippy::future_not_send
)]
#![forbid(unsafe_code, bare_trait_objects)]
#![cfg_attr(test, allow(non_snake_case, clippy::unwrap_used))]

mod error;
pub use error::{Error, Result};
use wasm_bindgen::prelude::*;

pub mod logic;
pub mod renderer;
pub mod web_app;

#[wasm_bindgen]
pub fn render_grid(width: u32, height: u32) {
    console_error_panic_hook::set_once();
    let app = web_app::RadialGridApp::new(width as u32, height as u32);
    app.window
        .container_document
        .body()
        .unwrap()
        .append_child(&app.window.render_surface.canvas)
        .unwrap();
    app.render();
}
