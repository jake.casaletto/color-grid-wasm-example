use wasm_bindgen::JsCast;
use web_sys::{window, CanvasRenderingContext2d, Document, HtmlCanvasElement};

pub struct CanvasRenderSurface {
    pub height: u32,
    pub width: u32,
    pub canvas: HtmlCanvasElement,
    pub context: CanvasRenderingContext2d,
}

impl CanvasRenderSurface {
    pub fn new(width: u32, height: u32, document: &Document) -> Self {
        let canvas = document
            .create_element("canvas")
            .expect("Couldn't make canvas")
            .dyn_into::<HtmlCanvasElement>()
            .expect("Could't cast canvas");
        canvas.set_width(width);
        canvas.set_height(height);
        canvas.style().set_property("border", "solid").unwrap();
        let context = canvas
            .get_context("2d")
            .unwrap()
            .unwrap()
            .dyn_into::<CanvasRenderingContext2d>()
            .unwrap();
        Self {
            height,
            width,
            canvas,
            context,
        }
    }
}

pub struct BrowserWindow {
    pub render_surface: CanvasRenderSurface,
    pub container_document: Document,
}

impl BrowserWindow {
    pub fn new(width: u32, height: u32) -> Self {
        let container_document = window().expect("Nope!").document().expect("AHHH");
        let render_surface = CanvasRenderSurface::new(width, height, &container_document);
        Self {
            container_document,
            render_surface,
        }
    }
}
